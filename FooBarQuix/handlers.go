package main

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"net/http"
	"strconv"
)

func statusHandler(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(`{"status":"ok"}`))
}

type versionResponse struct {
	Version string `json:"version"`
}

func versionHandler(w http.ResponseWriter, r *http.Request) {

	resultBytes, _ := json.Marshal(
		versionResponse{Version},
	)
	_, _ = w.Write(resultBytes)
}

type errorResponse struct {
	Error string `json:"error"`
}

type fooBarQuixResponse struct {
	Result string `json:"result"`
}

func fooBarQuixHandler(w http.ResponseWriter, r *http.Request) {
	number := chi.URLParam(r, "number")
	parsedNumber, err := strconv.ParseInt(number, 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		errBytes, _ := json.Marshal(errorResponse{Error: err.Error()})
		_, _ = w.Write(errBytes)
		return
	}

	resultBytes, _ := json.Marshal(
		fooBarQuixResponse{
			FooBarQuix(int(parsedNumber)),
		},
	)
	_, _ = w.Write(resultBytes)
}
