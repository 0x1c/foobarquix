package main

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func routes() *chi.Mux {

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.SetHeader("Content-Type", "application/json"))

	r.Get("/ready", statusHandler)
	r.Get("/version", versionHandler)
	r.Get("/{number}", fooBarQuixHandler)

	return r
}
