package main

import (
	"fmt"
	"testing"
)

func TestFooBarQuix(t *testing.T) {
	tests := []struct {
		input int
		want  string
	}{
		{1, "1"},
		{2, "2"},
		{3, "FooFoo"},
		{4, "4"},
		{5, "BarBar"},
		{6, "Foo"},
		{7, "QuixQuix"},
		{8, "8"},
		{9, "Foo"},
		{10, "Bar"},
		{51, "FooBar"},
		{53, "BarFoo"},
		{21, "FooQuix"},
		{13, "Foo"},
		{15, "FooBarBar"},
		{33, "FooFooFoo"},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("input=%d", tt.input), func(t *testing.T) {
			if got := FooBarQuix(tt.input); got != tt.want {
				t.Errorf("FooBarQuix(%d) = %s, want %v", tt.input, got, tt.want)
			}
		})
	}
}
