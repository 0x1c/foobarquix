FooBarQuix
===

Files
---

* `main.go` - entrypoint into the application, only responsible for starting the server.
* `routes.go` - All the routes are defined in this file. This is useful in larger projects to quickly get to the correct handler for a route.
* `handlers.go` - The two handlers are defined here.
* `foobarquix.go` - Implementation of the FooBarQuix algorithm.
  * `foobarquix_test.go` - Tests for the FooBarQuix function. All of the examples from the PDF document are here in a table test.
