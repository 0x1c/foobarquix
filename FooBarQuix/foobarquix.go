package main

import "strconv"

func FooBarQuix(input int) string {
	result := ""
	if input % 3 == 0 {
		result = result + "Foo"
	}
	if input % 5 == 0 {
		result = result + "Bar"
	}
	if input % 7 == 0 {
		result = result + "Quix"
	}
	digits := strconv.Itoa(input)
	for _, digit := range digits {
		switch digit {
		case '3':
			result = result + "Foo"
		case '5':
			result = result + "Bar"
		case '7':
			result = result + "Quix"
		}
	}
	if result == "" {
		return strconv.Itoa(input)
	}
	return result
}
