package main

import (
	"fmt"
	"log"
	"net/http"
)

const port = 80

// overwritten when building the docker image
var Version = "dev"

func main() {

	fmt.Printf("Starting server at port %d, version %s\n", port, Version)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", port), routes()); err != nil {
		log.Fatal(err)
	}
}
