Initial Setup
===

```bash
apt-get update && apt-get upgrade -y && apt-get install git make

git clone https://gitlab.com/0x1c/foobarquix.git

cd foobarquix

make setup
```
