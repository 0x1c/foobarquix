test:
	cd FooBarQuix && go test ./...

run:
	cd FooBarQuix && go run ./...

setup:
	snap install microk8s --classic
	snap install helm --classic
	microk8s.enable dns storage
	helm repo add stable https://kubernetes-charts.storage.googleapis.com/
	microk8s.kubectl apply -f kubernetes/jenkins/manifests.yaml
	helm install -n jenkins -f kubernetes/jenkins/values.yaml jenkins stable/jenkins